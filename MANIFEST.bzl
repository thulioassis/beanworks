project_manifest = struct(
    name = "beanworks",
    # gopath_prefix defines the path relative to the $GOPATH env var that
    # prefixes the project. It should not include the project directory.
    gopath_prefix = "bitbucket.org/thulioassis",
)
