# Beanworks

Beanworks repository is the mono-repo for the Beanworks products and supporting tools.

## Project dependencies

This project uses [Bazel](https://bazel.build/) as a general build tool. For installing it, follow
the instructions [here](https://docs.bazel.build/versions/master/install.html). If using Mac OS X,
prefer the Homebrew installation.

## Building the project

To build the entire project, run:

```txt
bazel build //...
```

## Running the services

### Identity service

Run the following command from the project root:

```txt
bazel run //services/identity
```

### Xero integration service

Run the following command from the project root:

```txt
OAUTH_CLIENT_KEY=<Xero consumer client key> \
OAUTH_CLIENT_SECRET=<Xero consumer client secret> \
bazel run //services/integrations/xero
```

#### Obtaining the Xero consumer keys

Go to [https://developer.xero.com](https://developer.xero.com) and register a new Public App.
The keys will be provided under the section `OAuth 1.0a Credentials`.

## Diagrams

### Authentication diagram

![Authentication diagram](./diagrams/authentication.svg)

### Accounts listing diagram

![Accounts listing diagram](./diagrams/accounts.svg)

## Getting started

Follow the next steps to interact with the system. All the communication will be with `curl`.
Make sure to get all services running as described above.

### Authenticating

Call the following command to authenticate onto the Beanworks system.

```txt
curl -X POST \
  -H 'Content-Type: application/json' \
  -d '{}' \
  http://localhost:3000/twirp/identity.User/Authenticate
```

### Enabling Xero integration

Call the following command to start the flow to enable the Xero integration.

```txt
curl -X POST \
  -H 'Content-Type: application/json' \
  -H 'Authorization: <Beanworks token>' \
  -d '{}' \
  http://localhost:3000/twirp/identity.User/EnableXero
```

- Use the `<Beanworks token>` from the response of the previous `Authenticate` call.

### Authorizing the OAuth app on Xero

- Navigate to the address contained in the `oauth_url` from the response of the previous
  `EnableXero` call using your browser.
- Allow the access.
- Copy the `Verification Code` to your clipboard.
- Call the following command to finish enabling the Xero integration:

```txt
curl -X POST \
  -H 'Content-Type: application/json' \
  -H 'Authorization: <Beanworks token>' \
  -d '{"verification_code": <verification code>, "uid_request_token": <UID request token>}' \
  http://localhost:3000/twirp/identity.User/VerifyCodeXero
```

- `<UID request token>` is the big token returned from the call to `Enabling Xero integration`.
- It will return a new token. Use it for the next command.

### Listing bank accounts

```txt
curl -X POST \
  -H 'Content-Type: application/json' \
  -H 'Authorization: <New Beanworks token>' \
  -d '{}' \
  http://localhost:3000/twirp/identity.User/ListBankAccounts
```

- `<New Beanworks token>` is the token returned from the previous command.
- If you want to write the output to disk, just pipe it to a file:

```txt
curl -X POST \
  -H 'Content-Type: application/json' \
  -H 'Authorization: <New Beanworks token>' \
  -d '{}' \
  http://localhost:3000/twirp/identity.User/ListBankAccounts \
  > accounts.json
```

## Business requirements

- Mono-repo for keeping sync of all external and internal dependencies.
- Bazel as a build tool to manage dependencies, package visibility, code generation
  (e.g. Protobuf code gen).
- Separation of concerns (SoC) principle for designing the services APIs.
- Dependency injection technique to make the business logic unit testable.
