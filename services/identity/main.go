package main

import (
	"net/http"

	"bitbucket.org/thulioassis/beanworks/services/identity/actions"
	"bitbucket.org/thulioassis/beanworks/services/identity/middlewares"
	"bitbucket.org/thulioassis/beanworks/services/identity/protos"
)

func main() {
	twirpHandler := middlewares.WithAuthorization(
		protos.NewUserServer(actions.NewDefaultUserServer(), nil),
	)
	mux := http.NewServeMux()
	mux.Handle(protos.UserPathPrefix, twirpHandler)
	http.ListenAndServe(":3000", mux)
}
