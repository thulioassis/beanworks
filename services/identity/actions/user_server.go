package actions

import (
	"context"
	"encoding/base64"
	"fmt"
	"net/http"
	"time"

	jwt "github.com/dgrijalva/jwt-go"
	"github.com/gogo/protobuf/proto"

	"bitbucket.org/thulioassis/beanworks/services/identity/database"
	"bitbucket.org/thulioassis/beanworks/services/identity/middlewares/headers"
	pb_user "bitbucket.org/thulioassis/beanworks/services/identity/protos/user"
	pb_xero "bitbucket.org/thulioassis/beanworks/services/identity/protos/user/integrations/xero"
	pb_xero_svc "bitbucket.org/thulioassis/beanworks/services/integrations/xero/protos"
)

const (
	tokenTTL = 30 * 24 * time.Hour // The time-to-live of the JWT since its creation time.
)

var (
	hmacSampleSecret = []byte("mysupersecretsamplekey")
)

// UserServer implements the User service contract.
type UserServer struct {
	xeroSvcClient pb_xero_svc.Xero
	database      database.Database
}

// NewDefaultUserServer constructs a new UserServer with the default service dependencies.
func NewDefaultUserServer() *UserServer {
	// TODO(f0rmiga): Make the address a const. It should also be the Kubernetes internal DNS name.
	xeroSvcClient := pb_xero_svc.NewXeroProtobufClient("http://localhost:3001", &http.Client{})

	db := database.New()

	return &UserServer{
		xeroSvcClient: xeroSvcClient,
		database:      db,
	}
}

// Authenticate authenticates a user onto the system.
func (s *UserServer) Authenticate(
	ctx context.Context,
	req *pb_user.AuthenticateReq,
) (*pb_user.AuthenticateRes, error) {
	// TODO(f0rmiga):
	// - Validate the request payload.
	// - Perform the database lookup.
	// - Check if credentials match.

	// - Generate Beanworks Token.
	claims := defaultClaims()
	claims["very_ficticious_user_id"] = "12345"
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(hmacSampleSecret)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_user.AuthenticateRes{
		Token: &pb_user.BeanworksToken{
			Data: tokenStr,
		},
	}

	return res, nil
}

// ListBankAccounts lists the bank accounts.
func (s *UserServer) ListBankAccounts(
	ctx context.Context,
	req *pb_user.ListBankAccountsReq,
) (*pb_user.ListBankAccountsRes, error) {
	// - Extract the Beansworks Token from the headers.
	tokenStr, ok := ctx.Value(headers.Authorization).(string)
	if !ok {
		return nil, fmt.Errorf("bad request: missing Authorization header")
	}

	// - Validate the Beanworks Token.
	token, err := jwt.Parse(tokenStr, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return hmacSampleSecret, nil
	})
	if err != nil {
		return nil, fmt.Errorf("bad request: the JWT is invalid")
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok {
		return nil, fmt.Errorf("bad request: the JWT is invalid")
	}

	// - Extract the integrations from the .
	// IMPORTANT! This part and the next are absolutely not optimal. But given the constraints of this
	// tech test, it solves the problem. I would give this part more love with the full architecture
	// of the system.
	integrations, ok := claims["integrations"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("bad request: missing integrations")
	}

	// - Call integrations.xero.svc/ListBankAccounts.
	xeroIntegration, ok := integrations["xero"].(map[string]interface{})
	if !ok {
		return nil, fmt.Errorf("bad request: missing xero integration")
	}
	xeroAccessTokenDataStr, ok := xeroIntegration["access_token"].(string)
	if !ok {
		return nil, fmt.Errorf("bad request: missing xero access_token")
	}
	xeroAccessTokenData, err := base64.RawStdEncoding.DecodeString(xeroAccessTokenDataStr)
	if err != nil {
		return nil, fmt.Errorf("bad request: wrong format for Xero access_token")
	}

	var xeroAccessToken pb_xero_svc.AccessToken
	err = proto.Unmarshal(xeroAccessTokenData, &xeroAccessToken)
	if err != nil {
		return nil, fmt.Errorf("bad request: wrong format for Xero access_token")
	}

	xeroListBankAccountsReq := &pb_xero_svc.ListBankAccountsReq{
		AccessToken: &xeroAccessToken,
	}
	xeroListBankAccountsRes, err := s.xeroSvcClient.ListBankAccounts(ctx, xeroListBankAccountsReq)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_user.ListBankAccountsRes{
		Xero: &pb_user.ListBankAccountsRes_Xero{
			BankAccounts: xeroListBankAccountsRes.BankAccounts,
		},
	}

	return res, nil
}

// EnableXero enables the Xero integration for a user/organization.
func (s *UserServer) EnableXero(
	ctx context.Context,
	req *pb_xero.EnableXeroReq,
) (*pb_xero.EnableXeroRes, error) {
	// TODO(f0rmiga):
	// - Extract the Beansworks Token from the headers.
	// - Validate the Beanworks Token.

	// - Call integrations.xero.svc/OAuthURL.
	oauthURLReq := &pb_xero_svc.OAuthURLReq{}
	oauthURLRes, err := s.xeroSvcClient.OAuthURL(ctx, oauthURLReq)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_xero.EnableXeroRes{
		OauthUrl:        oauthURLRes.OauthUrl,
		UidRequestToken: oauthURLRes.UidRequestToken,
	}

	return res, nil
}

// VerifyCodeXero completes an OAuth1 flow by verifying a code returned from the Xero API.
func (s *UserServer) VerifyCodeXero(
	ctx context.Context,
	req *pb_xero.VerifyCodeXeroReq,
) (*pb_xero.VerifyCodeXeroRes, error) {
	// TODO(f0rmiga):
	// - Extract the Beansworks Token from the headers.
	// - Validate the Beanworks Token.

	// - Call integrations.xero.svc/VerifyCode.
	verifyCodeReq := &pb_xero_svc.VerifyCodeReq{
		VerificationCode: req.VerificationCode,
		UidRequestToken:  req.UidRequestToken,
	}
	verifyCodeRes, err := s.xeroSvcClient.VerifyCode(ctx, verifyCodeReq)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Update the Xero integration flag to true and save the Access Token.
	err = s.database.Save(verifyCodeRes.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Generate a new Beanworks Token with the Xero Access Token.
	claims := defaultClaims()
	claims["very_ficticious_user_id"] = "12345"
	accessTokenData, err := proto.Marshal(verifyCodeRes.AccessToken)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}
	accessTokenDataStr := base64.RawStdEncoding.EncodeToString(accessTokenData)
	claims["integrations"] = map[string]interface{}{
		"xero": map[string]string{
			"access_token": accessTokenDataStr,
		},
	}
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	tokenStr, err := token.SignedString(hmacSampleSecret)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_xero.VerifyCodeXeroRes{
		Token: &pb_user.BeanworksToken{
			Data: tokenStr,
		},
	}

	return res, nil
}

func defaultClaims() jwt.MapClaims {
	iat := time.Now().UTC()
	nbf := iat.Add(-time.Second) // Ensures the token is valid if used right away.
	exp := iat.Add(tokenTTL)

	return jwt.MapClaims{
		"iat": iat.Unix(),
		"nbf": nbf.Unix(),
		"exp": exp.Unix(),
	}
}
