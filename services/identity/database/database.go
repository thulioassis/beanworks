package database

// Database is the interface that wraps the Save method for managing a database.
type Database interface {
	Save(...interface{}) error
}

// New constructs a default Database. For this tech test, it constructs a no-op database.
func New() Database {
	return &noopDatabase{}
}

// A no-op database.
type noopDatabase struct {
}

func (lc *noopDatabase) Save(...interface{}) error {
	return nil
}
