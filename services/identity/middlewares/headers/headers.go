package headers

type headerKey uint16

const (
	// Authorization represents the Authorization header.
	Authorization headerKey = iota
)
