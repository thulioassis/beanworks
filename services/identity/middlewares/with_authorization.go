package middlewares

import (
	"context"
	"net/http"

	"bitbucket.org/thulioassis/beanworks/services/identity/middlewares/headers"
)

// WithAuthorization is the middleware that populates the context with the Authorization header.
func WithAuthorization(handler http.Handler) http.Handler {
	return http.HandlerFunc(func(res http.ResponseWriter, req *http.Request) {
		auth := req.Header.Get("Authorization")
		if auth != "" {
			ctx := req.Context()
			ctx = context.WithValue(ctx, headers.Authorization, auth)
			newReq := req.WithContext(ctx)
			handler.ServeHTTP(res, newReq)
		} else {
			handler.ServeHTTP(res, req)
		}
	})
}
