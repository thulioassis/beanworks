package actions

import (
	"context"
	"crypto/rand"
	"crypto/tls"
	"encoding/hex"
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/gogo/protobuf/jsonpb"
	"github.com/mrjones/oauth"

	"bitbucket.org/thulioassis/beanworks/services/integrations/xero/lib/cache"
	pb_xero "bitbucket.org/thulioassis/beanworks/services/integrations/xero/protos"
)

const (
	baseURL           = "https://api.xero.com/oauth"
	requestTokenURL   = baseURL + "/RequestToken"
	authorizeTokenURL = baseURL + "/Authorize"
	accessTokenURL    = baseURL + "/AccessToken"
)

// XeroServer implements the Xero service contract.
type XeroServer struct {
	oauthClientKey    string
	oauthClientSecret string
	// TODO(f0rmiga): create an interface for the oauth.Consumer so its methods can be mocked for
	// testing.
	consumer *oauth.Consumer
	randRead func(b []byte) (n int, err error)
	cache    cache.Cache
}

// NewDefaultXeroServer constructs a new XeroServer with the default service dependencies.
func NewDefaultXeroServer() *XeroServer {
	oauthClientKey := os.Getenv("OAUTH_CLIENT_KEY")
	if oauthClientKey == "" {
		log.Fatal("Missing OAUTH_CLIENT_KEY environment variable")
	}

	oauthClientSecret := os.Getenv("OAUTH_CLIENT_SECRET")
	if oauthClientSecret == "" {
		log.Fatal("Missing OAUTH_CLIENT_SECRET environment variable")
	}

	serviceProvider := oauth.ServiceProvider{
		RequestTokenUrl:   requestTokenURL,
		AuthorizeTokenUrl: authorizeTokenURL,
		AccessTokenUrl:    accessTokenURL,
		HttpMethod:        "POST",
	}
	consumer := oauth.NewConsumer(oauthClientKey, oauthClientSecret, serviceProvider)
	consumer.HttpClient = &http.Client{
		Transport: &http.Transport{
			TLSClientConfig: &tls.Config{InsecureSkipVerify: true},
		},
	}

	cache := cache.New()

	return &XeroServer{
		oauthClientKey:    oauthClientKey,
		oauthClientSecret: oauthClientSecret,
		consumer:          consumer,
		cache:             cache,
		randRead:          rand.Read,
	}
}

// OAuthURL constructs a new OAuth URL and an UID for the request token.
func (s *XeroServer) OAuthURL(
	ctx context.Context,
	req *pb_xero.OAuthURLReq,
) (*pb_xero.OAuthURLRes, error) {
	// - Generate the OAuth URL.
	requestToken, oauthURL, err := s.consumer.GetRequestTokenAndUrl("oob")
	if err != nil {
		// TODO(f0rmiga): Log the error to STDERR here and elsewhere.
		return nil, fmt.Errorf("internal server error")
	}

	// - Generate the UID for the Request Token.
	uid := make([]byte, 512)
	_, err = s.randRead(uid)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	uidStr := hex.EncodeToString(uid)

	// - Save the UID for the Request Token in the cache.
	err = s.cache.Save(uidStr, requestToken)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_xero.OAuthURLRes{
		OauthUrl:        oauthURL,
		UidRequestToken: uidStr,
	}

	return res, nil
}

// VerifyCode completes an OAuth1 flow by verifying a code returned from the Xero API.
func (s *XeroServer) VerifyCode(
	ctx context.Context,
	req *pb_xero.VerifyCodeReq,
) (*pb_xero.VerifyCodeRes, error) {
	// - Get the Request Token from the cache using the UID.
	requestTokenInterface, err := s.cache.Load(req.UidRequestToken)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	requestToken := requestTokenInterface.(*oauth.RequestToken)

	// - Get the Access Token.
	accessToken, err := s.consumer.AuthorizeToken(requestToken, req.VerificationCode)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	// - Construct the response and send.
	res := &pb_xero.VerifyCodeRes{
		AccessToken: &pb_xero.AccessToken{
			Token:          accessToken.Token,
			Secret:         accessToken.Secret,
			AdditionalData: accessToken.AdditionalData,
		},
	}

	return res, nil
}

// ListBankAccounts lists the bank accounts on Xero.
func (s *XeroServer) ListBankAccounts(
	ctx context.Context,
	req *pb_xero.ListBankAccountsReq,
) (*pb_xero.ListBankAccountsRes, error) {
	accessToken := &oauth.AccessToken{
		Token:          req.AccessToken.Token,
		Secret:         req.AccessToken.Secret,
		AdditionalData: req.AccessToken.AdditionalData,
	}
	client, err := s.consumer.MakeHttpClient(accessToken)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	endpoint := "https://api.xero.com/api.xro/2.0/Accounts?where=Type==\"BANK\""
	bankAccountsReq, err := http.NewRequest("GET", endpoint, nil)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	bankAccountsReq.Header.Set("Accept", "application/json")

	bankAccountsRes, err := client.Do(bankAccountsReq)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}
	defer bankAccountsRes.Body.Close()

	if bankAccountsRes.StatusCode != 200 {
		return nil, fmt.Errorf("failed with code: %d", bankAccountsRes.StatusCode)
	}

	// - Construct the response and send.
	unmarshaler := &jsonpb.Unmarshaler{
		AllowUnknownFields: true,
	}
	res := &pb_xero.ListBankAccountsRes{}
	err = unmarshaler.Unmarshal(bankAccountsRes.Body, res)
	if err != nil {
		return nil, fmt.Errorf("internal server error")
	}

	return res, nil
}
