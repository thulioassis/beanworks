package cache

import "fmt"

// Cache is the interface that wraps the Save and Load methods for managing a fast key-value cache.
type Cache interface {
	Save(key string, val interface{}) error
	Load(key string) (val interface{}, err error)
}

// New constructs a default Cache. For this tech test, it constructs a non-concurrent, non-scalable
// local cache.
func New() Cache {
	return &localCache{
		data: make(map[string]interface{}),
	}
}

// A non-concurrent, non-scalable local cache.
type localCache struct {
	data map[string]interface{}
}

func (lc *localCache) Save(key string, val interface{}) error {
	_, exists := lc.data[key]
	if exists {
		return fmt.Errorf("key already exists")
	}
	lc.data[key] = val
	return nil
}

func (lc *localCache) Load(key string) (interface{}, error) {
	val, exists := lc.data[key]
	if !exists {
		return nil, fmt.Errorf("key not found")
	}
	return val, nil
}
