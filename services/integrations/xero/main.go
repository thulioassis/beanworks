package main

import (
	"net/http"

	"bitbucket.org/thulioassis/beanworks/services/integrations/xero/actions"
	"bitbucket.org/thulioassis/beanworks/services/integrations/xero/protos"
)

func main() {
	twirpHandler := protos.NewXeroServer(actions.NewDefaultXeroServer(), nil)
	mux := http.NewServeMux()
	mux.Handle(protos.XeroPathPrefix, twirpHandler)
	http.ListenAndServe(":3001", mux)
}
